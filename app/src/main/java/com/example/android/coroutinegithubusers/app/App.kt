package com.example.android.coroutinegithubusers.app

import android.app.Application
import com.example.android.coroutinegithubusers.utils.AppUtils
import com.facebook.stetho.Stetho

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        
        Stetho.initializeWithDefaults(this)
        AppUtils.init(this)
    }
}