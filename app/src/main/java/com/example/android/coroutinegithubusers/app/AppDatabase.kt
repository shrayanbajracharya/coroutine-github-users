package com.example.android.coroutinegithubusers.app

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.android.coroutinegithubusers.data.local.UserDao
import com.example.android.coroutinegithubusers.data.local.UserEntity
import com.example.android.coroutinegithubusers.utils.AppUtils

@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabaseInstance(): AppDatabase {
            synchronized(this) {
                // Synchronized to prevent multiple threads to access database instance
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        AppUtils.getApp().applicationContext,
                        AppDatabase::class.java,
                        "user_database"
                    ).build()
                }
                return instance!!
            }
        }
    }
}