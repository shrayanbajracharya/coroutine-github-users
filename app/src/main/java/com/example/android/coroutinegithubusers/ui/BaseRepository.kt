package com.example.android.coroutinegithubusers.ui

import com.example.android.coroutinegithubusers.app.AppDatabase

abstract class BaseRepository {

    fun getDatabase(): AppDatabase {
        return AppDatabase.getDatabaseInstance()
    }
}