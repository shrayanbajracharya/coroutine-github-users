package com.example.android.coroutinegithubusers.utils

import android.app.Application

class AppUtils {

    companion object {
        private lateinit var APP: Application

        fun init(app: Application) {
            if (!Companion::APP.isInitialized) {
                APP = app
            }
        }

        fun getApp(): Application {
            return APP
        }
    }
}