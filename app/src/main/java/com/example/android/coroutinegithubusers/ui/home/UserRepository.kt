package com.example.android.coroutinegithubusers.ui.home

import com.example.android.coroutinegithubusers.data.local.UserEntity
import com.example.android.coroutinegithubusers.ui.BaseRepository

class UserRepository private constructor() : BaseRepository() {

    companion object {
        private var instance: UserRepository? = null

        fun getRepositoryInstance(): UserRepository {
            return instance
                ?: UserRepository()
        }
    }

    private val userDao = getDatabase().userDao()

    suspend fun insertUser(user: UserEntity) {
        userDao.insertUser(user)
    }
}