package com.example.android.coroutinegithubusers.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.android.coroutinegithubusers.data.local.UserEntity
import kotlinx.coroutines.launch

class UserViewModel : ViewModel() {

    private var repository = UserRepository.getRepositoryInstance()

    fun insertUser(user: UserEntity) {
        viewModelScope.launch {
            repository.insertUser(user)
        }
    }
}