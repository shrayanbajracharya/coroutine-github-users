package com.example.android.coroutinegithubusers.data.local

import androidx.room.Entity
import androidx.room.PrimaryKey

private const val NO_ID = -1

@Entity(tableName = "user_table")
data class UserEntity(
    @PrimaryKey(autoGenerate = false)
    var id: Int = NO_ID,
    var name: String = "",
    var address: String = ""
)
